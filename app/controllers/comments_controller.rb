class CommentsController < ApplicationController
	before_action :set_post, only: [:show, :edit, :update, :destroy, :new, :create, :index]

	def new
		@comment = Comment.new
	end

	def index
		@comments = @post.comments.all
	end

	def create
		@comment = Comment.new(comment_params)
		@comment.post_id = @post.id
		@comment.user_id = current_user.id

		if @comment.save
			redirect_to @post
		else
			render 'new'
		end
	end

	def show
		@comment = Comment.find(params[:id])
	end

	def edit
		@comment = @post.comments.find(params[:id])
	end

	def update
		@comment = @post.comments.find(params[:id])
 		if @comment.update_attributes(params[:comment])
			@comment.post_id = @post.id
			@comment.user_id = current_user.id
			redirect_to 'root_url'
		else
			render 'edit'
		end
	end

	def destroy
		@comment = Comment.find(params[:id])
		@comment.destroy
	    flash[:success] = 'Comment was successfully destroyed.'
	    redirect_to root_url
	end

	private

		def comment_params
			params.require(:comment).permit(:rating, :content, :post_id, :user_id)
		end

		def set_post
			@post = Post.find(params[:post_id])
		end

end
