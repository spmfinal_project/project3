class UsersController < ApplicationController
  before_action :logged_in_user, only: [:index, :edit, :update, :destroy, :following, :followers]
  before_action :correct_user,   only: [:edit, :update]
  
  def index
    @users = User.paginate(page: params[:page])
  end

  def show
  	@user = User.find(params[:id])
    @random_users = User.where.not(id: @user).order("RANDOM()").limit(3)
    @posts = @user.posts.paginate(page: params[:page])
  end

  def new
  	@user = User.new
  end

  def create
  	@user = User.create(user_params)
  	if @user.save
      flash[:success] = "Signed up successfully! Welcome to the Chirp application"
      log_in @user
  		redirect_to @user
  	else
  		render 'new'
  	end
  end

  def edit
  	@user = User.find(params[:id])
  end

  def update
  	@user = User.find(params[:id])
  	if @user.update(user_params)
      flash[:info] = "User information updated successfully"
  		redirect_to @user
  	else
  		render 'edit'
  	end
  end

  def destroy
    @user = current_user
    @user.destroy
    log_out
    redirect_to root_url
  end

  def locate
    @users = User.all
    @hash = Gmaps4rails.build_markers(@users) do |user, marker|
      marker.lat user.latitude
      marker.lng user.longitude
      marker.infowindow user.first_name
    end
  end

  def search
    if params[:search].present?
      @users = User.search(params[:search])
    else
      @users = User.all
    end
  end

  def following
    @title = "Following"
    @user  = User.find(params[:id])
    @users = @user.following.paginate(page: params[:page])
    render 'show_follow'
  end

  def followers
    @title = "Followers"
    @user  = User.find(params[:id])
    @users = @user.followers.paginate(page: params[:page])
    render 'show_follow'
  end

  private

  	def user_params
  		params.require(:user).permit(:first_name, :last_name, :email, :gender, :dob, :location, :password, :password_confirmation, :profile_pic)
  	end

    def correct_user
      @user = User.find(params[:id])
      if (@user != current_user)
        flash[:danger] = "You have rights only to edit your information"
        redirect_to edit_user_path(current_user)
      end
    end

end
