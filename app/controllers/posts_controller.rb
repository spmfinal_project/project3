class PostsController < ApplicationController
	before_action :logged_in_user, only: [:create, :edit, :destroy, :upvote, :downvote, :show, :index]
	before_action :set_post, only: [:show, :edit, :update, :destroy, :upvote, :downvote]

	def create
		@post = current_user.posts.build(post_params)
		if @post.save
			flash[:success] = "Post Created Successfully!"
			redirect_to root_path
		else
			@feed_items = []
			render 'static_pages/home'
		end
	end

	def show
		@comments = @post.comments
	end

	def edit

	end

	def update
		if current_user.posts.update_all(post_params)
			flash.now[:info] = "Your Post Updated Successfully"
			redirect_to @post
		else
			render 'edit'
		end
	end

	def index
		@posts = Post.paginate(page: params[:page])
	end

	def destroy
		@post.delete
		flash.now[:warning] = "Your Post Deleted Successfully"
		redirect_to root_url
	end

	def upvote
		@post.upvote_by current_user
		redirect_to :back
	end

	def downvote
		@post.downvote_by current_user
		redirect_to :back
	end

	def my_posts
 		@user = User.find(params[:id])
 		@posts = @user.posts.paginate(page: params[:page])
 	end

	private

		def post_params
			params.require(:post).permit(:content, :avatar)
		end

		def set_post
			@post = Post.find(params[:id])
		end
end
