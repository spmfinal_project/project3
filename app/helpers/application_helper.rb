module ApplicationHelper

	def full_title(page_title = '')
		page_title.empty? ? "Chirp Application" : "#{page_title} | Chirp Application"
	end

end
