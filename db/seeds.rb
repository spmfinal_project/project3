# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
100.times do |n|
  first_name  = Faker::Name.first_name
  last_name  = Faker::Name.last_name
  email = "example-#{n+1}@railstutorial.org"
  password = "password"
  location = Faker::Address.city
  gender = "Male"
  dob = "19900907"
  User.create!(first_name:  first_name,
  			   last_name: last_name,
               email: email,
               password:              password,
               password_confirmation: password,
               location: location,
               gender: gender,
               dob: dob)
end

100.times do |n|
  content = Faker::Lorem.sentence
  user_id = rand(1..100)
  avatar = Faker::Avatar.image
  Post.create!(content: content,
               user_id: user_id,
               avatar: avatar)
end

1000.times do |n|
  rating = Faker::Number.between(1, 10)
  content = Faker::Hipster.sentences
  user_id = rand(1..100)
  post_id = rand(1..100)
  Comment.create!(rating: rating,
                  content: content,
                  user_id: user_id,
                  post_id: post_id)
end

users = User.all
user  = users.first
following = users[2..50]
followers = users[3..40]
following.each { |followed| user.follow(followed) }
followers.each { |follower| follower.follow(user) }