require 'test_helper'

class UserTest < ActiveSupport::TestCase
  def setup
  	@user = User.new(first_name: "Qwerty", last_name: "Kolla", email: "c_k35@txstate.edu", password: "Lak@1993", password_confirmation: "Lak@1993", location: "Kornepadu", gender: "Male", dob: "19900907")
  end

  test "Should be valid" do
  	assert @user.valid?
  end

  test "firstname should be present" do
  	@user.first_name = " "
  	assert_not @user.valid?
  end

  test "lastname should be present" do
  	@user.last_name = " "
  	assert_not @user.valid?
  end

  test "email should be present" do
  	@user.email = " "
  	assert_not @user.valid?
  end

  test "password should be present" do
  	@user.password = " "
  	assert_not @user.valid?
  end

  test "password confirmation should be present" do
  	@user.password_confirmation = ""
  	assert_not @user.valid?
  end

  test "password length should be minimum 8 characters" do
  	@user.password = "a" * 4
  	assert_not @user.valid?
  end

  test "password length should be greater than 8 characters" do

  end

end
