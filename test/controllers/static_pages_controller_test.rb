require 'test_helper'

class StaticPagesControllerTest < ActionController::TestCase
  
  def setup
  end
  
  test "should get home" do
    get :home
    assert_response :success
    assert_select "title", 'Home | Chirp Application'
  end

  test "should get about" do
    get :about
    assert_response :success
    assert_select "title", 'About | Chirp Application'
  end

  test "should get contact" do
    get :contact
    assert_response :success
    assert_select "title", 'Contactus | Chirp Application'
  end

  test "should get help" do
    get :help
    assert_response :success
    assert_select "title", 'Help | Chirp Application'
  end

end
